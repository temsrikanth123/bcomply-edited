// This is the updated project
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import {AdditionalDocument} from '../my-certification/my-certification.component';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  @Input() additionalDocuments: AdditionalDocument[];

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
  }

  response:any;
  fileToUpload:any;

  submit(files:FileList) {
    this.fileToUpload = files[0];
  }

  submitProcessForm(form:NgForm){

    const formData = new FormData();
    formData.append("file",this.fileToUpload)
    formData.append("description",form.form.value.description)
    formData.append("employeeCertificationTaskSavedDetailId","58")
    formData.append("employeeId","1100111")

    const call=this.http.post("http://localhost:8082/additional_document",formData);

    call.subscribe((response)=>{
      this.response=response;
      console.log(response)
    })
  }

}
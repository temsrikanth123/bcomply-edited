import { HttpClient } from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import {Remark} from '../my-certification/my-certification.component';

@Component({
  selector: 'app-remarks',
  templateUrl: './remarks.component.html',
  styleUrls: ['./remarks.component.css']
})
export class RemarksComponent implements OnInit {
  @Input() remarks: Remark[];

  constructor(private http:HttpClient) { }
  response:any
  
  ngOnInit(): void {

  }
  submitProcessForm(form:NgForm){
    const body={
      "employeeId":"1100111",
      "empCertificationTaskId":"64",
      "remarks": [
          {
              "id": 51,
              "empCertificationTaskId": 64,
              "empAssetDetailsId": "091230123",
              "isCompliant":form.value.compliance,
              "remarks":form.value.content 
          }
      ]
  }
    const call=this.http.post("http://localhost:8082/certifying/save/remarks",body);
    call.subscribe((response)=>{
      this.response=response;
      console.log(response)
    })

  }
}

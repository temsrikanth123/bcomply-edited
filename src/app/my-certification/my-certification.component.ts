import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';



@Component({
  selector: 'app-my-certification',
  templateUrl: './my-certification.component.html',
  styleUrls: ['./my-certification.component.css']
})
export class MyCertificationComponent implements OnInit {

  public certifier: Certifier;
  public reporter: Reporter;
  public empCertTask: EmployeeCertificationTask;
  public proc=false;
  public rem=false;
  public addi=false;
  public visibility=false;
  public popup=false
  
  constructor(private http: HttpClient) { }
  
  //popup=this.globals.popup;
  
  ngOnInit(): void {
    const requestObject = {
      certificateId: 64,
      employeeId: '1100111'
    };

    const certifierCertificationDetails = this.http.post('http://localhost:8080/certifying/certificationTask/certificateId', requestObject);

    certifierCertificationDetails.subscribe((response: CertificationDetails) => {
      console.log(response);
      this.certifier = response.certifier;
      this.reporter = response.reporter;
      this.empCertTask = response.empCertTask;
      this.empCertTask.savedDetails.remarks = this.addSerialNumber(this.empCertTask.savedDetails.remarks);
    });
  }

  // tslint:disable-next-line:typedef
  addSerialNumber(remarks: Remark[]) {
    for (let i = 0 ; i < remarks.length; i++) {
      remarks[i].serialNumber = i + 1;
    }
    return remarks;
  }
  actProcess(){
    this.rem=false;
    this.addi=false;
    this.proc=true;
    this.visibility=true
  }
  actRemarks(){
    this.proc=false;
    this.addi=false;
    this.rem=true;
    this.visibility=true;
  }
  actAdditional(){
    this.proc=false;
    this.rem=false;
    this.addi=true;
    this.visibility=true;
  }
  myFunction(){
    this.proc=false;
    this.rem=false;
    this.addi=false;
    this.visibility=false;
  }
}

export interface CertificationDetails {
  certifier: Certifier;
  reporter: Reporter;
  empCertTask: EmployeeCertificationTask;
}

export interface Certifier {
  employeeName: string;
  role: string;
  businessUnitName: string;
}

export interface Reporter {
  nameOfReporter: string;
  reporterRole: string;
  parentBusinessUnitName: string;
}

export interface EmployeeCertificationTask {
  id: number;
  employeeId: string;
  dueDate: Date;
  certifiedOn: Date;
  certifierName: string;
  certTask: CertificationTask;
  savedDetails: SavedDetails;
}

export interface CertificationTask {
  id: number;
  name: string;
  startDate: Date;
  endDate: Date;
  plannedStartDate: Date;
}

export interface SavedDetails {
  id: number;
  isCompliant: string;
  certificationStatus: string;
  empProcessAreas: EmpProcessArea[];
  remarks: Remark[];

  additionalDocuments: AdditionalDocument[];
  certificate: Certificate[];
}

export interface EmpProcessArea {
  id: number;
  processAreaId: number;
  employeeCertificationTaskId: number;
  empProcesses: EmpProcess[];
}

export interface EmpProcess {
  id: number;
  process: Process;
  isCompliant: string;
  severity: string;
  remarks: string;
}
export interface Process {
  processId: number;
  name: string;
}

export interface Remark {
  id: number;
  serialNumber: number;
  empCertificationTaskId: number;
  empAssetDetailsId: string;
  isCompliant: string;
  remarks: string;
}

export interface AdditionalDocument {
  id: number;
  empCertificationTaskDetailsId: number;
  path: string;
  description: string;
}

export interface Certificate {
  path: string;
  description: string;
  empCertificationTaskDetailsId: number;
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MyCertificationComponent } from './my-certification/my-certification.component';
import {HttpClientModule} from '@angular/common/http';
import { ProcessComponent } from './process/process.component';
import { RemarksComponent } from './remarks/remarks.component';
import { DocumentsComponent } from './documents/documents.component';
//import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { PreviewComponent } from './preview/preview.component';
import { DigitalComponent } from './digital/digital.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SuccessPopupComponent } from './success-popup/success-popup.component';

import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    MyCertificationComponent,
    ProcessComponent,
    RemarksComponent,
    DocumentsComponent,
    PreviewComponent,
    DigitalComponent,
    SuccessPopupComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    FileUploadModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  exports : [SuccessPopupComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

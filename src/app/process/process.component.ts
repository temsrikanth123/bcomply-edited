//this is updated code
import {HttpClient} from "@angular/common/http";
import {Component, Input, OnInit} from '@angular/core';
import {EmpProcessArea} from '../my-certification/my-certification.component';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit {
  @Input() empProcessAreas: EmpProcessArea[];
    response:any;
    compilance1:any
    RiskSeverity1:any
    remarks1:any
    compilance2:any
    RiskSeverity2:any
    remarks2:any
    compilance3:any
    RiskSeverity3:any
    remarks3:any
    compilance4:any
    RiskSeverity4:any
    remarks4:any
    compilance5:any
    RiskSeverity5:any
    remarks5:any
    
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
 

  } 
  submitProcessForm(form:NgForm){
    this.compilance1=form.value.compliance1;
    this.RiskSeverity1=form.value.RiskSeverity1;
    this.remarks1=form.value.remarks1;
    this.compilance2=form.value.compliance2;
    this.RiskSeverity2=form.value.RiskSeverity2;
    this.remarks2=form.value.remarks2;
    this.compilance3=form.value.compliance3;
    this.RiskSeverity3=form.value.RiskSeverity3;
    this.remarks3=form.value.remarks3;
    this.compilance4=form.value.compliance4;
    this.RiskSeverity4=form.value.RiskSeverity4;
    this.remarks4=form.value.remarks4;
    this.compilance5=form.value.compliance5;
    this.RiskSeverity5=form.value.RiskSeverity5;
    this.remarks5=form.value.remarks5;
    // const body={
    //   employeeId:"1100111",
    //   empCertificationTaskId:"64",
    //   empProcessAreas: [
    //           {
    //               id: 29,
    //               processAreaId: 25,
    //               employeeCertificationTaskId: 64,
    //               empProcesses: [
    //                   {
    //                       id: 30,
    //                       process: {
    //                           processId: 15,
    //                           name: " IP&E Process Area 1 Process 1"
    //                       },
    //                       isCompliant: this.compilance1,
    //                       severity: this.RiskSeverity1,
    //                       remarks: this.remarks1
    //                   },
    //                   {
    //                       id: 31,
    //                       process: {
    //                           processId: 16,
    //                           name: " IP&E Process Area 1 Process 2"
    //                       },
    //                       isCompliant:this.compilance2,
    //                       severity: this.RiskSeverity2,
    //                       remarks:this.remarks2
    //                   },
    //                   {
    //                       id: 32,
    //                       process: {
    //                           processId: 17,
    //                           name: " IP&E Process Area 1 Process 3"
    //                       },
    //                       isCompliant: this.compilance3,
    //                       severity: this.RiskSeverity3,
    //                       remarks:this.remarks3
    //                   }
    //               ]
    //           },
    //           {
    //               id: 33,
    //               processAreaId: 26,
    //               employeeCertificationTaskId: 64,
    //               empProcesses: [
    //                   {
    //                       id: 34,
    //                       process: {
    //                           processId: 18,
    //                           name: " IP&E Process Area 2 Process 1"
    //                       },
    //                       isCompliant: this.compilance4,
    //                       severity: this.RiskSeverity4,
    //                       remarks:this.remarks4
    //                   },
    //                   {
    //                       id: 35,
    //                       process: {
    //                           processId: 19,
    //                           name: " IP&E Process Area 2 Process 2"
    //                       },
    //                       isCompliant: this.compilance5,
    //                       severity: this.RiskSeverity5,
    //                       remarks:this.remarks5
    //                   }
    //               ]
    //           }
    //       ]
    // }
    const body={
        "employeeId":"1100111",
        "empCertificationTaskId":64,
        "empProcessAreas": [
                {
                    "id": 28,
                    "processArea": {
                        "id": 25,
                        "name": "Intellectual Property and Engineering Processes"
                    },
                    "employeeCertificationTaskId": 64,
                    "empProcesses": [
                        {
                            "id": 29,
                            "process": {
                                "processId": 15,
                                "name": " Necessary Filling completed for Trademark, Patent, Copyright and/or Design as per applicable"
                            },
                            "isCompliant": this.compilance1,
                            "severity": this.RiskSeverity1,
                            "remarks": this.remarks1
                        },
                        {
                            "id": 30,
                            "process": {
                                "processId": 16,
                                "name": " IPR filling done in relevant jurisdiction where TCS IP asset is to be positioned"
                            },
                            "isCompliant": this.compilance2,
                            "severity": this.RiskSeverity2,
                            "remarks": this.remarks2
                        },
                        {
                            "id": 31,
                            "process": {
                                "processId": 17,
                                "name": " Any deviation notified in the assessment report for this TCS IP asset has been actioned to meet the IP safe requirement"
                            },
                            "isCompliant": this.compilance3,
                            "severity": this.RiskSeverity3,
                            "remarks": this.remarks3
                        },
                        {
                            "id": 32,
                            "process": {
                                "processId": 18,
                                "name": " Asset has been verified for avoidance of any IP containment and infringement - where applicable(or recommended by IP&E team or TCS legal team) - through MCD and FTO"
                            },
                            "isCompliant": this.compilance4,
                            "severity": this.RiskSeverity4,
                            "remarks": this.remarks4
                        },
                        {
                            "id": 33,
                            "process": {
                                "processId": 19,
                                "name": " All legal issues are resolved for this TCS IP asset"
                            },
                            "isCompliant": this.compilance5,
                            "severity": this.RiskSeverity5,
                            "remarks": this.remarks5
                        }
                    ]
                }
            ]
}
        
    
    const call=this.http.post("http://localhost:8082/certifying/save/processes",body);
    call.subscribe((response)=>{
      this.response=response;
      console.log(response)
    })
  }
  resetFormValue(processForm: NgForm){
    processForm.reset();
  }
  
}

import { Component, OnInit, Injectable} from '@angular/core';
import { Subscriber, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SuccessPopupComponent } from '../success-popup/success-popup.component';
//import { FileUploader } from 'ng2-file-upload';

@Injectable({
  providedIn: 'root'})

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {
  title = 'preview';
  popup=true;
  Response;
  certificatepdf;
  today: number =  Date.now();
  //pdf = new jspdf('p', 'mm', 'a4');
  certificatefile;

  constructor(private http: HttpClient,public dialog: MatDialog ){}

  public convetToPDF()
{
var data = document.getElementById('contentToConvert');
html2canvas(data).then(canvas => {
var imgWidth = 208;
var pageHeight = 295;
var imgHeight = canvas.height * imgWidth / canvas.width;
var heightLeft = imgHeight;
 
const contentDataURL = canvas.toDataURL('image/png')
let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
var position = 0;
pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
pdf.save('Certifcate to Board_John Smith_111111_dated 30 Dec 2020.pdf'); // Generated PDF
//this.certificatepdf=pdf.html(document.getElementById("contentToConvert"), {
  //callback: function (doc) {
    //this.certificatefile=doc.output('blob');
    //console.log(doc);
    //console.log(this.certificatefile);
  //}})
//this.certificatepdf
//this.certificatefile=pdf.output('blob');
});
}


  ngOnInit(): void {
  const requestData={
      certificateId:"64",
      employeeId:"1100111"
    }
    
   this.http.post<any>("http://localhost:8080/certifying/certificationTask/certificateId",requestData).
   subscribe(data=>{
     this.Response=data;
     console.log(data)
    }
   )
  }
  
  response:any;
  fileToUpload:any;

  submitProcessForm(){

    const formData = new FormData();
    formData.append("certificateTaskId","64")
    formData.append("empId","1100111")
    formData.append("certificate",this.certificatefile)
    

    const call=this.http.post("http://localhost:8080/generate",formData);

    call.subscribe((response)=>{
      this.response=response;

      console.log(response)
    })
  }
  
  onSubmit(){
    const dialogRef = this.dialog.open(SuccessPopupComponent,
      {
        width : '400px'
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  /*Submit(){
    console.log(this.pdf);
     this.http.post<any>("http://localhost:8080/generate",68,this.requestData.employeeId,this.pdf).
     subscribe(data=>{
       this.certificatepdf=data;
       console.log(data)
      }
     )
   }*/
}